
FROM fedora:32
LABEL maintainer Daryll Strauss <daryll.strauss@gmail.com>

RUN dnf install -y podman buildah

RUN adduser test
USER test
